from django.shortcuts import redirect, render

try:
    from recipes.forms import RecipeForm
    from recipes.models import Recipe
except Exception:
    RecipeForm = None
    Recipe = None


def create_recipe(request):
    if request.method == "POST" and RecipeForm:  # for submitted with info
        form = RecipeForm(request.POST)  # set form to submitted info
        if form.is_valid():  # if vlaid
            recipe = form.save()  # save recipe var to saved form
            return redirect("recipe_detail", pk=recipe.pk)  # set
    elif RecipeForm:
        form = RecipeForm()
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "recipes/new.html", context)


def change_recipe(request, pk):
    if Recipe and RecipeForm:  #
        instance = Recipe.objects.get(pk=pk)  # instance = specific pk recipe
        if request.method == "POST":  #
            form = RecipeForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect(
                    "recipe_detail", pk=pk
                )  # pk=pk keyword arg for get() -- left is getting assigned to right from fn arg above
        else:
            form = RecipeForm(instance=instance)
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "recipes/edit.html", context)


def show_recipes(request):
    context = {  # context is a var assigned to a dict
        "recipes": Recipe.objects.all()  # "recipies" is a key available in HTML as a var
        if Recipe
        else [],  # pull from database, if Recipe exists, else show empty list
    }
    return render(
        request, "recipes/list.html", context
    )  # render is a fn, takes url request with html file and provides context var


def show_recipe(request, pk):
    context = {
        "recipe": Recipe.objects.get(pk=pk) if Recipe else None,
    }
    return render(request, "recipes/detail.html", context)
