from django.db import models


# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField("")
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name + " by " + self.author


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.abbreviation


class FoodItem(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    amount = models.FloatField()
    recipe = models.ForeignKey(
        "Recipe",
        related_name="ingredients",
        on_delete=models.CASCADE,  # many ingredients belongs to one recipe
    )

    measure = models.ForeignKey(
        "Measure",
        on_delete=models.PROTECT,  # many ingredients belongs to one measure
    )

    food = models.ForeignKey("FoodItem", on_delete=models.PROTECT)

    def __str__(self):
        return self.amount


class Step(models.Model):
    recipe = models.ForeignKey(
        "Recipe", related_name="steps", on_delete=models.CASCADE
    )
    order = models.SmallIntegerField()
    directions = models.CharField(max_length=300)
    food_items = models.ManyToManyField(
        "FoodItem", related_name="steps", null=True, blank=True
    )

    def __str__(self):
        return self.directions

    # step.food_items = QuerySet[
    FoodItem
    # a step instance -> print out a set of fooditems
    # add to bowl : lettuce, onion, other veggies
    # food_item.steps = QuerySet[
    # Step


# a fooditem instance _> print out a set of steps
# for the onion: cut in half, cut radially, add to bowl
